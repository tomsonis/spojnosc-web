# SpojnoscWeb

W celu uruchomienia należy w głównym katalogu projektu wykonanać poniższe komendy:

 * npm install
 * ng serve

# Opis działania aplikacji

WAŻNE!!! Po każdym odświeżeniu przeglądarki należy kliknąć "Clear all rumor".

Na ekranie każde kółko reprezentuje jeden serwer. Jeśli serwer jest uruchomiony 
poprawnie ma kolor zielony, w innym przypadku(zatrzymany, zamrożony) kolor czerwony. Mniejsze kółko oznacza
spójność serwera. Tutaj istnieją dwa sposoby na okreslanie spójności:
  * spójny jest wtedy, gdy serwer zachowuje ciągłość wiadomości od samego początku
  * spójny jest wtedy, gdy serwer posiada tylko ostatnią przesłaną wiadomość
  
W przypadku pierwszego rozwiązania serwer nie będzie spójny, gdy: 
  * serwer 1 zostanie zamrożony
  * wiadomość zostanie rozesłana
  * serwer 1 zostanie odmrożony

Wysłana wiadomość nigdy nie dotrze do tego serwera i do samego końca testowania będzie niespójny.

W drugim przypadku jest możliwość, aby serwery były spójne gdy zostaną zamrożone oraz odmrożone.
Nie wiem co tutaj 
Akcje jakie można wykonać na danym serwerze klikając LPM myszy na kółku:
  * Wyświetlić jego wiadomości
  * Zamrozić lub odmrozić
  * Wyłączyć(nie da się z web właczyć)
  
"Send rumor" - wysyła plotkę na losowy serwer po czym on przesyła dalej
  


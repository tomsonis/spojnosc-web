export class ServerModel {
  id: string;
  port: number;
  freezed: boolean;
  consistent = false;


  constructor(id: string, port: number, freezed: boolean) {
    this.id = id;
    this.port = port;
    this.freezed = freezed;
  }
}

export class ServerData {
  name: string;
  data: string;

  constructor(name: string, data: string) {
    this.name = name;
    this.data = data;
  }
}

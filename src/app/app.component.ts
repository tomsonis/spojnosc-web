import {Component} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ServerData, ServerModel} from './model/app.model';
import {FreezedRespone, FreezeRequest} from './model/response.model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

const HOST = 'http://localhost';
const FIRST_PORT = 5000;
const SERVER_SIZE = 11;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  servers = new Array<ServerModel>();
  serverDataMap = new Map<string, Array<ServerData>>();
  rumorInterval: number;
  sentMessage = new Array<ServerData>();

  private httpClient: HttpClient;
  private modalService: NgbModal;

  constructor(
    httpClient: HttpClient,
    modalService: NgbModal
  ) {
    this.httpClient = httpClient;
    this.modalService = modalService;
    this.initialize();
    this.checkStatusTimer();
    this.checkConsistentTimer();
    this.sentMessage = [
      {name: 'srv1', data: '1'},
      {name: 'srv2', data: '2'},
      {name: 'srv3', data: '3'},
      {name: 'srv4', data: '4'},
      {name: 'srv5', data: '5'},
      {name: 'srv6', data: '6'},
      {name: 'srv7', data: '7'},
      {name: 'srv8', data: '8'},
      {name: 'srv9', data: '9'},
      {name: 'srv10', data: '10'},
      {name: 'srv11', data: '11'},
    ];
  }

  initialize() {
    for (let i = 0; i < SERVER_SIZE; i++) {
      const serverName = `srv${i + 1}`;
      this.servers.push(new ServerModel(serverName, FIRST_PORT + i, true));
    }
  }

  checkStatusTimer() {
    setInterval(() => {
      this.servers.forEach(server => {
        this.httpClient.get(`${HOST}:${server.port}/freeze/`).subscribe((response: FreezedRespone) => {
          server.freezed = response.freezed;
        });
      });
    }, 1000);
  }

  checkConsistentTimer() {
    setInterval(() => {
      this.servers.forEach(server => {
        this.httpClient.get(`${HOST}:${server.port}/alldata/`).subscribe((response: Array<ServerData>) => {
          server.consistent = this.hasConsistMessage(response);
        });
      });
    }, 1000);
  }

  private hasConsistMessage(response: Array<ServerData>): boolean {
    if (response.length !== this.sentMessage.length) {
      return false;
    }

    for (const message of response) {
      if (this.sentMessage.find(value => value === message) === null) {
        return false;
      }
    }
    return true;
  }

  unFrozze(server: ServerModel) {
    this.httpClient.post(`${HOST}:${server.port}/freeze/`, new FreezeRequest('False')).subscribe(() => {
    }, error => console.log(error));
  }

  frozze(server: ServerModel) {
    this.httpClient.post(`${HOST}:${server.port}/freeze/`, new FreezeRequest('True')).subscribe(() => {
    }, error => console.log(error));
  }

  kill(server: ServerModel) {
    this.httpClient.get(`${HOST}:${server.port}/kill/`).subscribe(() => {
      this.servers = this.servers.filter(item => item !== server);
    }, error => console.log(error));
  }

  killAll() {
    this.servers.forEach(server => {
      this.kill(server);
    });
  }

  sendRandomRumor() {
    this.rumorInterval = setInterval(() => {
      console.log('rumooor');
    }, 300);
  }

  stopRumor() {
    clearInterval(this.rumorInterval);
    this.rumorInterval = null;
  }

  showServerData(content, server: ServerModel) {
    this.serverDataMap.clear();

    this.httpClient.get(`${HOST}:${server.port}/alldata/`).subscribe((response: Array<ServerData>) => {
      this.serverDataMap.set(server.id, response);
    });

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then(() => {
    }, () => {
    });
  }

  clearRumors() {
    this.servers.forEach(server => {
      this.httpClient.delete(`${HOST}:${server.port}/alldata/`).subscribe(() => {
      });
    });

    this.sentMessage = new Array<ServerData>();
  }

  sendRumor() {
    let randomPort: number;
    let randomData: number;
    const serverModels = this.servers.filter(value => value.freezed === false);

    if (serverModels.length === 0) {
      console.log('Cant send message. All server are freezed.');
      return;
    }

    let findServer: ServerModel = null;
    do {
      randomPort = Math.floor((Math.random() * 11) + 5000);
      randomData = Math.floor(Math.random() * 999999);

      console.log(randomPort);
      findServer = this.servers.find(server => server.port === randomPort && server.freezed === false);
    } while (findServer == null);



    this.httpClient.post(`${HOST}:${randomPort}/data/`, {data: `${randomData}`}).subscribe(() => {
      this.sentMessage.push({name: findServer.id, data: String(randomData)});

      console.log(this.sentMessage);
    }, error => console.log(error));
  }
}
